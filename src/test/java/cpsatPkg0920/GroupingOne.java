package cpsatPkg0920;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class GroupingOne {
	
  @Test (groups = "category 1", priority = 1)
  public void COneOne() {
	  System.out.println("this is category 1 priority 1");
  }
  
  @Test (groups = "category 1", priority = 0)
  public void COneZero() {
	  System.out.println("this is category1 priority 0");
  }
  
  @Test (groups = "category 1", priority = -1)
  public void COneMinus() {
	  System.out.println("this is category 1 priority -1");
  }
  
  @Test (groups = "category 2", priority = 1)
  public void CTwoOne() {
	  System.out.println("this is category2 priority 1");
  }
  
  @Test (groups = "category 2", priority = -1)
  public void CTwoMinus() {
	  System.out.println("this is category 2 priority -1");
  }
  
  
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
