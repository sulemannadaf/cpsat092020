package cpsatPkg0920;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RediffTopStories {
	WebDriver driver;
	WebDriverWait wait;
	
  @Test
  public void f() {
	  List<WebElement> topstr = driver.findElements(By.xpath("//*[@id=\"topdiv_0\"]/h2[*]/a"));
	  for (WebElement e : topstr) {
		  System.out.println(e.getAttribute("href"));
	  }  
  }
  
  @Test
  public void g() {
	  
	  driver.findElement(By.xpath("//*[@class='movies']/a[contains(text(),'MOVIES')]")).click();
	  List<WebElement> submenu = driver.findElements(By.xpath("/html/body/div[5]/div[2]/ul/li[*]"));
	  for (WebElement e : submenu) {
		  System.out.println(e.getAttribute("href"));
	  }  
  }
  
  @BeforeClass
 // @Parameters({"propertyName"})
  public void beforeClass() {
	  String browser = System.getProperty("propertyName");
	  driver = utils.HelperFunctions.createAppropriateDriver(browser);
	  driver.get("https://www.rediff.com/");
	  driver.manage().window().maximize();
	  wait = new WebDriverWait(driver, 10);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }

  @AfterClass
  public void afterClass() {
	  
  }

}
