package cpsatPkg0920;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class GroupingTwo {
 
	
	  @Test (groups = "Group 2 category 1", priority = 1)
	  public void COneOne() {
		  System.out.println("this is category 1 priority 1");
	  }
	  
	  @Test (groups = "Group 2 category 1", priority = 0)
	  public void COneZero() {
		  System.out.println("this is category1 priority 0");
	  }
	  
	  @Test (groups = "Group 2 category 1", priority = -1)
	  public void COneMinus() {
		  System.out.println("this is category 1 priority -1");
	  }
	  
	  @Test (groups = "Group 2 category 2", priority = 1)
	  public void CTwoOne() {
		  System.out.println("this is category2 priority 1");
	  }
	  
	  @Test (groups = "Group 2 category 2", priority = -1)
	  public void CTwoMinus() {
		  System.out.println("this is category 2 priority -1");
	  }
	  
	  
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("this is beforeMethod");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("this is AfterMethod");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("this is beforeTest");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("this is AfterTest");
  }

}
