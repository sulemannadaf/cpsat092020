package mockExams;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Q2 {

	WebDriver driver;
	WebDriverWait wait;
	

	String filePath = "C:\\Users\\5284385\\OneDrive - MyFedEx\\Desktop\\CP-SAT\\TestData\\nifty.xlsx";
	String writtenFile = "C:\\Users\\5284385\\OneDrive - MyFedEx\\Desktop\\TEMP\\Test.xlsx";

	int rownm = 1;
	int colnm = 3;
	FileInputStream fis ;
	//XSSFWorkbook workbook ;
	//XSSFSheet newsheet;
	//Row row;
	//Cell cell;

	@Test(dataProvider = "dp")
	public void f(String menu, String sub, String hrefs) throws IOException {

		// Set excel sheet
		
		  fis = new FileInputStream(writtenFile); 
		  XSSFWorkbook workbook = new XSSFWorkbook(fis);
		  XSSFSheet newsheet = workbook.getSheet("data");
		 
				
		//System.out.println("Test Items-\n"+ "Menu: "+menu+"\nsub: "+sub+"\nExpected href: "+hrefs);	
		List<WebElement> subMenu = driver.findElements(By.xpath("/html/body/div[5]/div[2]/ul/li[*]/a"));
		
		for (WebElement e: subMenu) {
			String actualHref = e.getAttribute("href");
			System.out.println("actual href: "+actualHref);
			System.out.println("Expected href"+hrefs);
			
			Row row = newsheet.createRow(rownm);
			Cell cell = row.createCell(3);// colnm
//			cell.setCellType(Cell.CELL_TYPE_STRING);
//			
			if (actualHref. contains(hrefs)) {
				System.out.println("actual href matches expected");
				
//				cell.setCellValue("PASS");
//				FileOutputStream fos = new FileOutputStream(writtenFile);
//				workbook.write(fos);
//				fos.close();
//				System.out.println("END OF WRITING DATA IN EXCEL");
//
				//rownm = rownm+1;
				break;
			}
			//else {
				//System.out.println("href not on page");
//				cell.setCellValue(actualHref+" -FAILED");
//				FileOutputStream fos = new FileOutputStream(writtenFile);
//				workbook.write(fos);
//				fos.close();
//				System.out.println("END OF WRITING DATA IN EXCEL");
//
//				rownm = rownm+1;

			//}

			/* WRITE TO A FILE */

			//		 FileInputStream fis = new FileInputStream(filePath);
			//		 XSSFWorkbook workbook = new XSSFWorkbook(fis);
			//		 XSSFSheet newsheet = workbook.getSheet("data");

			//		 Row row = newsheet.createRow(rownm);
			//		 Cell cell = row.createCell(colnm);
			//		 
			//		 cell.setCellType(cell.CELL_TYPE_STRING);
			//		 cell.setCellValue(actualHref);
			//		 FileOutputStream fos = new FileOutputStream(writtenFile);
			//		 workbook.write(fos);
			//		 fos.close();
			//		 System.out.println("END OF WRITING DATA IN EXCEL");
			//		 
			//		 rownm = rownm+1;


		}

	}

	@DataProvider
	public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		String [][] excelData = utils.XLDataReaders.getExcelData(filePath, "data");
		System.out.println("Number of records in excel = "+excelData.length); 
		for (int i=0;i<excelData.length;i++) {
			System.out.println("Data in sheet: "+Arrays.toString(excelData[i]));
		}
		return excelData;
	}
	
	@BeforeClass
	public void beforeClass() throws IOException {
		String browser = System.getProperty("propertyName");
		driver = utils.HelperFunctions.createAppropriateDriver(browser);
		driver.get("https://www.rediff.com/");
		driver.findElement(By.xpath("/html/body/div[5]/ul/li[2]")).click();
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 10);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


		
	}

	@AfterClass
	public void afterClass() {
	}

}
