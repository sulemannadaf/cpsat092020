package mockExams;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Q1 {
	WebDriver driver;
	WebDriverWait wait;
	
//  @Test (groups = "exam 1")
//  public void tooltip() throws InterruptedException {
//	  driver.findElement(By.xpath("/html/body/div[5]/ul/li[2]/a")).click();
//	  Actions hover = new Actions(driver);
//	  System.out.println(driver.findElement(By.
//			  xpath("//*[@class = \"navbar relative\"]/ul/li/a[contains(@href,'business')]")).getAttribute("title"));
//	  hover.moveToElement(driver.findElement(By.
//			  xpath("//*[@class = \"navbar relative\"]/ul/li/a[contains(@href,'business')]"))).perform();
//	  Thread.sleep(3000);
//  }
  
  
  @Test (groups = "exam 3")
  public void movies_color () {
	  driver.findElement(By.xpath("/html/body/div[5]/ul/li[4]/a")).click();
	  List<WebElement> moviesubs = driver.findElements(By.xpath("//*[@class='subnavbar movies']/ul/li[*]/a"));
	  for (WebElement e: moviesubs) {
		  System.out.println("sub: "+e.getAttribute("href"));
	  }
	  
	  String rgbcolor = driver.findElement(By.xpath("/html/body/div[5]/div[2]")).getCssValue("background");
	  System.out.println("RGB color: "+rgbcolor);
	  //String r = rgbcolor.replaceAll("[^\\d.]", "");
	  String rgbnew = rgbcolor.substring(rgbcolor.indexOf("(")+1, rgbcolor.indexOf(")"));
	  String[] hex = rgbnew.split(",");
	  System.out.println("R: "+hex[0]);
	  System.out.println("G: "+hex[1]);
	  System.out.println("B: "+hex[2]);
	  
	  driver.get("https://www.color-blindness.com/color-name-hue/");
	  driver.findElement(By.xpath("//*[@id=\"cp1_Red\"]")).sendKeys(hex[0]);
	  driver.findElement(By.xpath("//*[@id=\"cp1_Green\"]")).sendKeys(hex[1]);
	  driver.findElement(By.xpath("//*[@id=\"cp1_Blue\"]")).sendKeys(hex[2]);
	  System.out.println("color: "+driver.findElement(By.xpath("//*[@id=\"cp1_ColorNameText\"]")).getText());
	  
  }
  
  
  @BeforeClass
  public void beforeClass() {
	  String browser = System.getProperty("propertyName");
	  driver = utils.HelperFunctions.createAppropriateDriver(browser);
	  driver.get("https://www.rediff.com/");
	  driver.manage().window().maximize();
	  wait = new WebDriverWait(driver, 10);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }

  @AfterClass
  public void afterClass() {
  }

}
