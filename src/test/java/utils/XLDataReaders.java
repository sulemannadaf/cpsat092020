package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class XLDataReaders {

	/*
	 * This function takes two parameters
	 * 1. Excel file name
	 * 2. Sheet name
	 * Returns a two dimensional String Array
	 * This function uses PoI library to read the excel sheet
	 */
	public static String[][] getExcelData(String fileName, String sheetName)  throws EncryptedDocumentException,  IOException, InvalidFormatException {

		String[][] arrayExcelData = null;
		//Workbook wb = WorkbookFactory.create(new File(fileName));
		Workbook wb = WorkbookFactory.create(new FileInputStream(fileName));
		Sheet sh = wb.getSheet(sheetName);

		int totalNoOfRows = sh.getPhysicalNumberOfRows();
		int totalNoOfCols = sh.getRow(0).getPhysicalNumberOfCells();


		System.out.println("totalNoOfRows="+totalNoOfRows+","
				+ " totalNoOfCols="+totalNoOfCols);

		arrayExcelData = 
				new String[totalNoOfRows-1][totalNoOfCols];

		for (int i= 1 ; i <= totalNoOfRows-1; i++) {
			for (int j=0; j <= totalNoOfCols-1; j++) {
				Cell c = sh.getRow(i).getCell(j);
				DataFormatter df = new DataFormatter();
				String strdata = df.formatCellValue(c);

				arrayExcelData[i-1][j] = strdata;	
			}	
		}

		return arrayExcelData;	
	}

}
