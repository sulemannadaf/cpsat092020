package utils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HelperFunctions {
	/*
	 * This function takes two parameters
	 * driver to look at the existing windows
	 * title - title of the window where you want to switch to
	 * it returns false if there is no window with this title
	 * it returns true if the window is found and the switch is successful
	 */
	public static boolean switchToWindow(WebDriver driver, String title)
	{
		boolean flag = false;

		Set<String> iter = driver.getWindowHandles();

		int numHandles = iter.size();

		Object str[] = iter.toArray();


		for (int i=0;i<numHandles;i++)
		{
			System.out.println("window" + i + " handle name" + str[i]);

			String handle = (String)str[i];

			driver.switchTo().window(handle);

			String strTitle = driver.getTitle();
			if (strTitle.contains(title))
			{
				System.out.println("we are at right window");
				flag = true;
				break;
			}

		}
		return flag;

	}

	public static void switchToWindowbyNum(WebDriver driver, int windowNum)
	{
		Set<String> iter = driver.getWindowHandles();

		Object str[] = iter.toArray();

		String handle = (String)str[windowNum];

		driver.switchTo().window(handle);

			System.out.println("Title of window "+windowNum+" is - "+driver.getTitle());
	}

	public static WebDriver createAppropriateDriver(String browserName)
	{
		WebDriver driver;
		browserName = browserName.toUpperCase();

		switch (browserName) {

		case "CHROME":

			System.setProperty("webdriver.chrome.driver", "C:\\Users\\5284385\\OneDrive - MyFedEx\\Desktop\\TAF\\Workspace1\\cpsat092020\\src\\test\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();	
			break;

		case "FIREFOX":
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\5284385\\OneDrive - MyFedEx\\Desktop\\TAF\\Workspace1\\cpsat092020\\src\\test\\resources\\drivers\\geckodriver.exe");
			driver = new FirefoxDriver();	
			break;
			
		case "EDGE":
			System.setProperty("webdriver.edge.driver", "src\\test\\resources\\drivers\\MicrosoftWebDriver.exe");
			driver = new EdgeDriver();	
			break;			
		default:
			//create chrome by default
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();	
			break;
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		return driver;
	} // end of createAppropriateDriver(browserName)

	public static WebDriver createAppropriateDriver(String browserName, boolean headless)
	{
		WebDriver driver;
		ChromeOptions chromeOptions = new ChromeOptions();

		browserName = browserName.toUpperCase();

		switch (browserName) {

		case "CHROME":

			chromeOptions = new ChromeOptions();
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");

			chromeOptions.setHeadless(headless);

			driver = new ChromeDriver(chromeOptions);	
			break;

		case "FIREFOX":
			FirefoxOptions ffOptions = new FirefoxOptions();
			System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");		
			ffOptions.setHeadless(headless);
			driver = new FirefoxDriver(ffOptions);	
			break;
		case "EDGE":
			//EdgeOptions edgeoptions = new EdgeOptions();
			System.setProperty("webdriver.edge.driver", "src\\test\\resources\\drivers\\MicrosoftWebDriver.exe");
			// EdgeDriver Does not support headless
			// https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/14057655/
			// edgeoptions.setHeadless(headless);
			// run the test in non-headless mode
			System.out.println("Running Edge Browser in head mode, as headless is not supported");
			driver = new EdgeDriver();
			break;
		default:
			//create chrome by default
			chromeOptions = new ChromeOptions();
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
			chromeOptions.setHeadless(headless);

			driver = new ChromeDriver(chromeOptions);	
			break;
		}
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		return driver;
	} // end of createAppropriateDriver(browserName,headless)

	public static void captureScreenShot(WebDriver driver, String filename)
	{
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	// Following method returns the browser name
	// other values are being printed on the console

	public static String getBrowserName (WebDriver driver) {

		String strBrowserName = null;

		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		strBrowserName = cap.getBrowserName().toLowerCase();

		System.out.println(strBrowserName);
		String os = cap.getPlatform().toString();
		System.out.println(os);
		String v = cap.getVersion();
		System.out.println(v);
		return strBrowserName;
	}
	
	/*
	 * Following function is wrapper around for click
	 * it does two things
	 * 1. checks if the element is clickable or not
	 * 2. if it is then tries to stop the page load
	 * 3. then uses the standard click
	 * 4. returns true if the click is successful
	 */
	public static boolean elementClick(WebDriver driver, WebElement element) {

		// Wait for the element to be clickable

		WebDriverWait wait = new WebDriverWait(driver,30);

		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			// continue even if the page is not loaded
			driver.findElement(By.tagName("body")).sendKeys("Keys.ESCAPE");
			//driver.findElement(byObject).click();
			element.click();
			System.out.println("Clicked on " + element);
			return true;

		}
		catch (Exception e) {
			System.out.println("Not able to click on " + element);
			System.out.println(e.getMessage());
			return false;
		}
	} // end of elementClick

	static public List <WebElement> getElementList(WebDriver driver, By byObj) {

		List <WebElement> listObj = null;
		WebDriverWait wait = new WebDriverWait(driver,30);
		// wait for all the elements to be visible
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy( byObj));

		listObj = driver.findElements(byObj);

		if (listObj != null) {
			System.out.println("list size = " + listObj.size());
			System.out.println(listObj.get(0).getText());
		}
		return listObj;
	} // end of getElementList	

	public static void popupHandler(WebDriver driver, WebDriverWait wait ) {
		try {
			// //*[@id="page"]
			By popup1 = By.xpath("//*[@id='page']");
			wait.until(ExpectedConditions.visibilityOfElementLocated(popup1));
			WebElement wePopup1 = driver.findElement(popup1);
			wePopup1.click();
			wePopup1.sendKeys(Keys.ESCAPE);
			System.out.println("##Successfully handled popup1 - method2");

		}catch(Exception e1) {

			System.out.println("##No popup1 displayed ##");
			System.out.println("Exception " + e1.getMessage());
			System.out.println("##No popup1 displayed##");			

		}
		
	}
	
	public static void findTableValue(WebDriver driver) {
		
		 List<WebElement> c1v = driver.findElements(By.xpath("//tr/th[contains(text(),'')]"));  //** //*[@id=\"leftcontainer\"]/table/thead/tr/th
		  
		  for (int i=0;i<c1v.size();i++) {
			  System.out.println(c1v.get(i).getText());
			  i = i+1;
			  List<WebElement> rv = driver.findElements(By.xpath("//tr[contains(text(),'')]/td["+i+"]"));  ////*[@id=\"leftcontainer\"]/table/tbody/tr[*]/td["+i+"]
			  System.out.println("RV size : "+rv.size());
			  i = i-1;
			  for (int a=0;a<rv.size();a++) {
				  System.out.println(rv.get(a).getText());
			  } 
		  }
		
	}
	
}

